# Language Analysis

See what languages are use in Crossref metadata.

Click on the button below to launch the notebook.


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Flanguage_analysis/master?filepath=index.ipynb)